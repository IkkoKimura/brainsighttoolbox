## Brain Sight Toolbox

scripts to get and analyze the output from Brain Sight

this code requires Veta-Toolbox(https://www.frontiersin.org/articles/10.3389/fnins.2019.00975/full)
See example directory for the example usage of this toolbox 

## AUTHOR
Ikko Kimura, Osaka Univeristy, 2020/06/18

## UPDATES
Ikko Kimura, Osaka University, 2020/08/30, changed a lot for simplicity

## HOW TO PUSH
https://qiita.com/ecznhcy/items/31532ad7b6d7205b42e5
