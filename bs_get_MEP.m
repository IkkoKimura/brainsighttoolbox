function bs_get_MEP(idir,iname,odir,oname,nrows)

% get MEP data compatible with Veta toolbox

% Ikko Kimura, Osaka University, 2020/06/08
% Ikko Kimura, 2020/07/16, modified a bit
% Ikko Kimura, 2020/08/30, no need to specify the number of headers

%%% TO DO
% 

%%% 0. PARAMETERS
%idir='/home/ikimura/work/TBS_MRI/Test/sub-03';
%iname='20200605EMG.txt'; % input file name
%odir='/home/ikimura/work/TBS_MRI/Test/sub-03/EMG'; % output directory 
%oname='Session1.mat';
if nargin<5
    nrows=39:639;
if nargin<4
    oname='MEP.mat';
if nargin<3
    odir=fullfile(idir,'EMG');
end
end
end

%%% 1. GET DATA
Data=readcell(fullfile(idir,iname),'Delimiter',{';','\t'});
%%% read data
disp('Reading data...')
flag=0;
for i=1:size(Data,1)
    % check the comment
    if contains(Data{i,1},'#')
        % if Sample Name is there, start reading
        if  strcmp(Data{i,1},'# Sample Name')
            flag=1;
        else
            flag=0;
        end
    else % no comment 
        % read only MEP things
        if flag==1
           t=Data{i,3};
           MEP{t,1}=t;
           MEP{t,2}=double(cell2mat(Data(i,nrows)));
        end
    end
end

%%%2. CONFIRMATION
a1=length(MEP);
a2=length(MEP{1,2});
if a2~=601 % the number of timepoints must be 601
    fprintf('something wrong with the time points \n')
    fprintf('current time points: %s \n',num2str(a2))
    fprintf('expected time points: 601 \n')
else
    disp('timepoints seemed to be nice')
end
fprintf('num of Session: %s \n',num2str(a1))

%%%3. CONVERT TO VETA FORMAT
disp('Converting to Veta format...')
trials=array2table(MEP,'VariableNames',{'sweep_num','ch1'});

parameters.EMG=0;
parameters.EMG_burst_channels=0;
parameters.MEP=1;
parameters.artchan_index=1;
parameters.MEP_channels=1;
parameters.CSP=0;
parameters.CSP_channels=0;
parameters.sampling_rate=3000;
parameters.num_channels=1;

subject.ID=0;
subject.handedness='r';
subject.sex='m';
subject.offset=0;

% save the data
fprintf('Saving the data to %s \n',odir)
%mkdir(fullfile(odir))
save(fullfile(odir,'full_data.mat'),'Data','trials','parameters','subject')
save(fullfile(odir,oname),'trials','parameters','subject')
