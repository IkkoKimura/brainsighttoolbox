% the example script to get and analyze MEP data from Brain Sight

% Ikko Kimura, Osaka University, 2020/06/08
% Ikko Kimura, Osaka University, 2020/08/30, changed a lot for simplicity and generalizability
% Ikko Kimura, Osaka University, 2020/09/17, added param.time
% Ikko Kimura, Osaka University, 2020/09/19, bs_analyse_MEP exclude the sweep if needed

%%% TO DO
% readcell is only for R2019b

clear
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% !!! MODIFY THIS PART !!!
subj='002';
DataDir='/home/ikko/work/TMS/';
param.idir=fullfile(DataDir,'data');
param.iname=[subj,'_Session1.txt']; % input file name(all of the variables must be read out)

%% output directory
param.odir=fullfile(DataDir,subj,'EMG'); % change the folder for multiple sessions

param.int=[175:515];
param.sess={175:204,205:234,235:264,265:294,295:324,325:354,355:362,363:392,393:423,424:453,454:484,485:515};

%% elapsed time
%param.time=[0 5:5:30 40 50 60]; 
param.time=[0:5:45 55:5:60]; 
%param.time=[0:5:60];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%0. PREP
mkdir(param.odir)
%%%1. PREPROCESSS 
bs_get_MEP(param.idir,param.iname,param.odir) % get emg data
findEMGBrainSight(param.odir,'MEP.mat') % preprocess MEP Area--> zeros
bs_qc_MEP(param.odir,param.int) % get needed data, then little qc
%%%2. VISUAL INSPECTION 
visualizeEMG % should see 'MEP_preprocessed2.mat'
%%%% FIRST RUN ABOVE! %%%%%

% THEN RUN BELOW 
%%%3. ANALYSE EMG waveform and plot
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% !!! CHNAGE THIS ACCORDINGLY !!! 
param.exclude=[]; % 変なデータがあればここで指定して削除
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bs_analyse_MEP(param.odir,param.sess,'MEP_preprocessed2_visualized.mat',param.time,param.exclude)

save(fullfile(param.odir,'bs_param.mat'),'param') % save the prameter file just in case...

