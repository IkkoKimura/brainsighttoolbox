function Std=get_std(x)
% here is the little code to get the real std
% Ikko Kimura, Osaka Universtiy, 2020/06/08


thr_z=3;

%%% remove the outliers
n_old=length(x);
x_z=zscore(x);
x(abs(x_z)>thr_z)=[]; 
n=length(x);
if n_old>n
    fprintf('%s outliers were removed for calculation..\n',num2str(n_old-n));
end

%%% calculation
Std=sqrt(sum(x.^2)/(n-1));

end