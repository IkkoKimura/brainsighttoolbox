function bs_qc_MEP(odir,int,iname)

% quick data viz for qc and save only needed data
% Ikko Kimura, Osaka University, 2020/08/30
% Ikko Kimura, Osaka University, 2020/09/17, removed dataviz steps because
% this causes confusion with the next visualiseEMG step..

if nargin<3
iname='MEP_preprocessed.mat';
end
load(fullfile(odir,iname)) 
trials=trials(int,:);
save(fullfile(odir,'MEP_preprocessed2.mat'),'trials','subject','parameters')

% dataviz for qc
%f=cell2mat(trials.ch1);
%figure()
%plot((1/3)*[1:601]-50,f','Color',[0.8 0.8 0.8])
%hold on
%plot((1/3)*[1:601]-50,mean(f),'Color',[0 0 0],'LineWidth',2)
%box off
%set(gca,'FontSize',12)
